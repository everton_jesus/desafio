CREATE DATABASE webjump
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

CREATE TABLE `categorias` (
	`id_categoria` INT(11) NOT NULL AUTO_INCREMENT,
	`nomeCategoria` VARCHAR(100) NOT NULL,
	`code` DECIMAL(10,0) NULL DEFAULT NULL,
	INDEX `id_categoria` (`id_categoria`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;


CREATE TABLE `produtos` (
	`id_produto` INT(11) NOT NULL AUTO_INCREMENT,
	`nome` VARCHAR(250) NOT NULL DEFAULT '0',
	`sku` VARCHAR(100) NOT NULL DEFAULT '0',
	`pdo_quantidade` VARCHAR(50) NULL DEFAULT '0',
	`pdo_descricao` TEXT NULL,
	`pdo_preco` VARCHAR(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id_produto`),
	INDEX `id_produto` (`id_produto`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;


CREATE TABLE `produtos_categorias` (
	`id_pro_cat` INT(11) NOT NULL AUTO_INCREMENT,
	`produto_id` INT(11) NOT NULL DEFAULT '0',
	`categoria_id` INT(11) NOT NULL DEFAULT '0',
	INDEX `id_pro_cat` (`id_pro_cat`),
	INDEX `FK_produtos_categorias_produtos` (`produto_id`),
	INDEX `FK_produtos_categorias_categorias` (`categoria_id`),
	CONSTRAINT `FK_produtos_categorias_categorias` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id_categoria`),
	CONSTRAINT `FK_produtos_categorias_produtos` FOREIGN KEY (`produto_id`) REFERENCES `produtos` (`id_produto`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0
;

CREATE TABLE `imagens` (
	`id_image` INT(11) NOT NULL AUTO_INCREMENT,
	`produto_id` INT(11) NOT NULL,
	`imagem` VARCHAR(250) NOT NULL DEFAULT '0',
	INDEX `id_image` (`id_image`)
)
ENGINE=InnoDB
;

