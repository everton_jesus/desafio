<?php

namespace App\Controllers;

use App\Lib\Sessao;
use App\Models\DAO\ProdutoDAO;
use App\Models\Entidades\Produto;
use App\Models\Validacao\ProdutoValidador;
use App\Lib\Conexao;

class ProdutoController extends Controller
{
    public function index()
    {
        $produtoDAO = new ProdutoDAO();

        self::setViewParam('listaProdutos',$produtoDAO->listar());

        $this->render('/produto/index');

        Sessao::limpaMensagem();
    }

    public function cadastro()
    {
        $produtoDAO = new ProdutoDAO();

        self::setViewParam('listarCategorias',$produtoDAO->listarCategorias());

        $this->render('/produto/cadastro');

        Sessao::limpaFormulario();
        Sessao::limpaMensagem();
        Sessao::limpaErro();
    }

    public function salvar()
    {
        $Produto = new Produto();
        $Produto->setSku($_POST['sku']);
        $Produto->setNome($_POST['nome']);
        $Produto->setPreco($_POST['preco']);
        $Produto->setQuantidade($_POST['quantidade']);
        $Produto->setDescricao($_POST['descricao']);
        $Produto->setCategoria($_POST['categoria']);


        Sessao::gravaFormulario($_POST);

        $produtoValidador = new ProdutoValidador();
        $resultadoValidacao = $produtoValidador->validar($Produto);

        if($resultadoValidacao->getErros()){
            Sessao::gravaErro($resultadoValidacao->getErros());
            $this->redirect('/produto/cadastro');
        }

        $produtoDAO = new ProdutoDAO();

        $gravouProduto = $produtoDAO->salvar($Produto);

        if($gravouProduto == true){
             $Produto->setSku($_POST['sku']);
            $produtoDAO->salvarCategoriaProduto($Produto);
        }
        
        Sessao::limpaFormulario();
        Sessao::limpaMensagem();
        Sessao::limpaErro();

        $this->redirect('/produto');
      
    }
    
    public function edicao($params)
    {
        $id = $params[0];

        $produtoDAO = new ProdutoDAO();

        $produto = $produtoDAO->listar($id);

        if(!$produto){
            Sessao::gravaMensagem("Produto inexistente");
            $this->redirect('/produto');
        }

        self::setViewParam('produtos',$produto);
        self::setViewParam('listarCategorias',$produtoDAO->listarCategorias());

        $this->render('/produto/editar');

        Sessao::limpaMensagem();

    }

    public function atualizar()
    {

        $Produto = new Produto();
        $Produto->setId($_POST['id_produto']);
        $Produto->setNome($_POST['nome']);
        $Produto->setPreco($_POST['preco']);
        $Produto->setQuantidade($_POST['quantidade']);
        $Produto->setDescricao($_POST['descricao']);
        $Produto->setSku($_POST['sku']);

        Sessao::gravaFormulario($_POST);

        $produtoValidador = new ProdutoValidador();
        $resultadoValidacao = $produtoValidador->validar($Produto);

        if($resultadoValidacao->getErros()){
           
            Sessao::gravaErro($resultadoValidacao->getErros());
            $this->redirect('/produto/edicao/'.$_POST['id_produto']);
        }

        $produtoDAO = new ProdutoDAO();

        $produtoDAO->atualizar($Produto);

        Sessao::limpaFormulario();
        Sessao::limpaMensagem();
        Sessao::limpaErro();

        $this->redirect('/produto');

    }
    
    public function exclusao($params)
    {

        $id = $params[0];

        $produtoDAO = new ProdutoDAO();

        $produto = $produtoDAO->listar($id);

        if(!$produto){
            Sessao::gravaMensagem("Produto inexistente");
            $this->redirect('/produto');
        }

        self::setViewParam('produto',$produto);

        $this->render('/produto/exclusao');

        Sessao::limpaMensagem();

    }

    public function excluir()
    {
        $Produto = new Produto();
        $Produto->setId($_POST['id_produto']);

        $produtoDAO = new ProdutoDAO();

        if(!$produtoDAO->excluir($Produto)){
            Sessao::gravaMensagem("Produto inexistente");
            $this->redirect('/produto');
        }

        Sessao::gravaMensagem("Produto excluido com sucesso!");

        $this->redirect('/produto');

    }


    public function importar(){

    //conecto via mysqli_connect na base, apenas para não usar o PDO neste momento
    $conn = mysqli_connect("localhost", "root", "", "webjump");
    
    $fileName = $_FILES["file"]["tmp_name"];
    
        if ($_FILES["file"]["size"] > 0) {
            
            $file = fopen($fileName, "r");
            $count  = 1;
            while (($column = fgetcsv($file, 10000, ";")) !== FALSE) {

                 if  ($count >= 2)  {

                    $sqlInsert = mysqli_query($conn, "INSERT into produtos (nome,sku,pdo_descricao,pdo_quantidade,pdo_preco)
                           values ('" . $column[0] . "','" . $column[1] . "','" . $column[2] . "','" . $column[3] . "','" . $column[4] . "')");
                    $id_produto = mysqli_insert_id($conn);                    

                    echo "ID do Produto: " . $id_produto; 
                
                    if(!empty($id_produto) && !empty($column[5])){
                        $array = explode('|', $column[5]);

                        foreach($array as $key => $categoria){

                        $sqlInsertCategoria = mysqli_query($conn, "INSERT into categorias (nomeCategoria)
                               values ('" . $categoria . "')");

                        $id_categoria = mysqli_insert_id($conn);

                        echo "ID Categoria: " . $id_categoria; 

                        if(!empty($id_produto) && !empty($id_categoria)){
                            $sqlInsertProdCat = mysqli_query($conn, "INSERT into produtos_categorias (produto_id, categoria_id)
                                   values (" . $id_produto . "," . $id_categoria . " )");                        
                        }
                        
                        }

                    }


                 }

                  $count++; 

            }
        }

    }

}