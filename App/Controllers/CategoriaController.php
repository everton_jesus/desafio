<?php

namespace App\Controllers;

use App\Lib\Sessao;
use App\Models\DAO\CategoriaDAO;
use App\Models\Entidades\Categoria;
use App\Models\Validacao\CategoriaValidador;

class CategoriaController extends Controller
{
    public function index()
    {
        $categoriaDAO = new CategoriaDAO();

        self::setViewParam('listaCategorias',$categoriaDAO->listar());

        $this->render('/categoria/index');

        Sessao::limpaMensagem();
    }

    public function cadastro()
    {
        $this->render('/categoria/cadastro');

        Sessao::limpaFormulario();
        Sessao::limpaMensagem();
        Sessao::limpaErro();
    }

    public function salvar()
    {
        $Categoria = new Categoria();
        
        $Categoria->setNomeCategoria($_POST['nomeCategoria']);
        $Categoria->setCode($_POST['code']);


        Sessao::gravaFormulario($_POST);

        $categoriaValidador = new CategoriaValidador();
        $resultadoValidacao = $categoriaValidador->validar($Categoria);

        if($resultadoValidacao->getErros()){
            
            Sessao::gravaErro($resultadoValidacao->getErros());
            $this->redirect('/categoria/cadastro');
        }

        $categoriaDAO = new CategoriaDAO();

        $categoriaDAO->salvar($Categoria);
        
        Sessao::limpaFormulario();
        Sessao::limpaMensagem();
        Sessao::limpaErro();

        $this->redirect('/categoria');
      
    }
    
    public function edicao($params)
    {
        $id = $params[0];

        $categoriaDAO = new CategoriaDAO();

        $categoria = $categoriaDAO->listar($id);

        if(!$categoria){
            Sessao::gravaMensagem("Categoria inexistente");
            $this->redirect('/categoria');
        }

        self::setViewParam('categoria',$categoria);

        $this->render('/categoria/editar');

        Sessao::limpaMensagem();

    }

    public function atualizar()
    {

        $Categoria = new Categoria();
        $Categoria->setId($_POST['id_categoria']);
        $Categoria->setNomeCategoria($_POST['nomeCategoria']);
        $Categoria->setCode($_POST['code']);

        Sessao::gravaFormulario($_POST);

        $categoriaValidador = new CategoriaValidador();
        $resultadoValidacao = $categoriaValidador->validar($Categoria);

        if($resultadoValidacao->getErros()){
           
            Sessao::gravaErro($resultadoValidacao->getErros());
            $this->redirect('/categoria/edicao/'.$_POST['id_categoria']);
        }

        $categoriaDAO = new CategoriaDAO();

        $categoriaDAO->atualizar($Categoria);

        Sessao::limpaFormulario();
        Sessao::limpaMensagem();
        Sessao::limpaErro();

        $this->redirect('/categoria');

    }
    
    public function exclusao($params)
    {
        $id = $params[0];

        $categoriaDAO = new CategoriaDAO();

        $categoria = $categoriaDAO->listar($id);

        if(!$categoria){
            Sessao::gravaMensagem("Categoria inexistente");
            $this->redirect('/categoria');
        }

        self::setViewParam('categoria',$categoria);

        $this->render('/categoria/exclusao');

        Sessao::limpaMensagem();

    }

    public function excluir()
    {
        $Categoria = new Categoria();
        $Categoria->setId($_POST['id_categoria']);

        $categoriaDAO = new CategoriaDAO();

        if(!$categoriaDAO->excluir($Categoria)){
            Sessao::gravaMensagem("Categoria inexistente");
            $this->redirect('/categoria');
        }

        Sessao::gravaMensagem("Categoria excluido com sucesso!");

        $this->redirect('/categoria');

    }
}