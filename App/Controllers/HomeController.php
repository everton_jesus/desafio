<?php

namespace App\Controllers;

use App\Lib\Sessao;
use App\Models\DAO\ProdutoDAO;
use App\Models\Entidades\Produto;
use App\Models\Validacao\ProdutoValidador;

class HomeController extends Controller
{
    public function index()
    {

        $produtoDAO = new ProdutoDAO();

        self::setViewParam('listaProdutos',$produtoDAO->listar());

        $this->render('home/index');
    }
}