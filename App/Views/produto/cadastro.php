<!-- Main Content -->
<main class="content">
    <h1 class="title new-item">New Product</h1>
    
    <form action="http://<?php echo APP_HOST; ?>/produto/salvar" method="post" id="form_cadastro">
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" name="sku" id="sku" class="input-text" required="" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" name="nome" id="name" class="input-text" required="" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" name="preco" id="price" class="input-text" onkeypress='return SomenteNumero(event)' required="" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" name="quantidade" id="quantity" class="input-text" required="" onkeypress='return SomenteNumero(event)'/> 
      </div>
      <?php if(!count($viewVar['listarCategorias'])){ ?>
     <div class="input-field">
        <p><b>Adicione uma categoria primeiro! <a herf="http://<?php echo APP_HOST; ?>/categoria/cadastro"></a></b></p>
    </div>
    <?php } else { ?>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple name="categoria[]" id="category" class="input-text" required="">
            <?php foreach($viewVar['listarCategorias'] as $categoria) { ?>
                    <option value="<?php echo $categoria->getId();?>"><?php echo $categoria->getNomeCategoria(); ?></option>
            <?php } ?>       
        </select>
      </div>
      <?php } ?>
      
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea name="descricao"id="description" class="input-text" required=""></textarea>
      </div>
      <div class="actions-form">
        <a href="http://<?php echo APP_HOST; ?>/produto" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>
      
    </form>
  </main>
  <script language='JavaScript'>
  function SomenteNumero(e){
      var tecla=(window.event)?event.keyCode:e.which;   
      if((tecla>47 && tecla<58)) return true;
      else{
        if (tecla==8 || tecla==0) return true;
    else  return false;
      }
}
</script>
  <!-- Main Content -->