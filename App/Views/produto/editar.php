<!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Product</h1>
    
    <form action="http://<?php echo APP_HOST; ?>/produto/atualizar" method="post" id="form_cadastro">
        <input type="hidden" class="form-control" name="id_produto" id="id" value="<?php echo $viewVar['produtos']->getId(); ?>">
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" name="sku" id="sku" class="input-text" value="<?php echo $viewVar['produtos']->getSku(); ?>" required/> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" name="nome" id="name" class="input-text" value="<?php echo $viewVar['produtos']->getNome(); ?>" required/> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" name="preco" id="price" class="input-text" value="<?php echo $viewVar['produtos']->getPreco(); ?>" required/> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" name="quantidade" id="quantity" class="input-text" value="<?php echo $viewVar['produtos']->getQuantidade(); ?>" required/> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple name="categoria[]" id="category" class="input-text" value="" required>
          <?php if(!count($viewVar['listarCategorias'])){ ?>
            <option value="1">Category 1</option>
            <option value="2">Category 2</option>
            <option value="3">Category 3</option>
            <option value="4">Category 4</option>
          <?php } ?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea name="descricao" id="description" class="input-text"><?php echo $viewVar['produtos']->getDescricao(); ?></textarea>
      </div>
      <div class="actions-form">
        <a href="http://<?php echo APP_HOST; ?>/produto" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->