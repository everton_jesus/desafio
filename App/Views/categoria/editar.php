 <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">New Category</h1>
    
   <form action="http://<?php echo APP_HOST; ?>/categoria/atualizar" method="post" id="form_cadastro">
    <input type="hidden" class="form-control" name="id_categoria" id="id" value="<?php echo $viewVar['categoria']->getId(); ?>">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" name="nomeCategoria" id="category-name" class="input-text" value="<?php echo $viewVar['categoria']->getNomeCategoria(); ?>" required />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" name="code" id="category-code" class="input-text" value="<?php echo $viewVar['categoria']->getCode(); ?>" required />
        
      </div>
      <div class="actions-form">
        <a href="categories.html" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->
