  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      You have <?php echo count($viewVar['listaProdutos']);?> products added on this store: <a href="http://<?php echo APP_HOST; ?>/produto/cadastro" class="btn-action">Add new Product</a>     
    </div>
    <br>
    <div class="infor">
      <form action="http://<?php echo APP_HOST; ?>/produto/importar" method="post" id="form_cadastro" enctype="multipart/form-data" >
      <p><b>Importar Produtos</b> <input type="file" name="file"></p>
      <button type="submit"> Importar Produtos</button>  
      </form>
    </div>
        <?php
            if(!count($viewVar['listaProdutos'])){
        ?>
            <div class="alert alert-info" role="alert">Nenhum produto encontrado</div>
        <?php
            } else {
        ?>
          <ul class="product-list">
              <?php
                  foreach($viewVar['listaProdutos'] as $produto) { ?>
                <li>
                  <div class="product-image">
                    <img src="assets/images/product/go-logo.png" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
                  </div>
                  <div class="product-info">
                    <div class="product-name"><span><?php echo $produto->getNome(); ?></span></div>
                    <div class="product-price"><span class="special-price"><?php echo $produto->getQuantidade(); ?> available</span> <span>R$ <?php echo number_format($produto->getPreco(), 2);?>
                    </span></div>
                  </div>
                </li>
            <?php } ?>
            </ul>  <?php } ?>
  </main>
  <!-- Main Content -->