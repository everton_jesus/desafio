<?php

namespace App\Models\Entidades;

use DateTime;

class Produto
{
    private $id_produto;
    private $nome;
    private $sku;
    private $preco;
    private $pdo_quantidade;
    private $descricao;
    private $categoria;
    private $image;
    

    public function getId()
    {
        return $this->id_produto;
    }

    public function setId($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }


    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function getPreco()
    {
        return $this->pdo_preco;
    }

    public function setPreco($preco)
    {
        $this->pdo_preco = $preco;
    }

    public function getQuantidade()
    {
        return $this->pdo_quantidade;
    }

    public function setQuantidade($pdo_quantidade)
    {
        $this->pdo_quantidade = $pdo_quantidade;
    }

    public function getDescricao()
    {
        return $this->pdo_descricao;
    }

    public function setDescricao($descricao)
    {
        $this->pdo_descricao = $descricao;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


}