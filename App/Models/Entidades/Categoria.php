<?php

namespace App\Models\Entidades;

use DateTime;

class Categoria
{
    private $id_produto;
    private $nome;
    private $code;
    

    public function getId()
    {
        return $this->id_categoria;
    }

    public function setId($id_categoria)
    {
        $this->id_categoria = $id_categoria;
    }

    public function getNomeCategoria()
    {
        return $this->nomeCategoria;
    }

    public function setNomeCategoria($nome)
    {
        $this->nomeCategoria = $nome;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }
 

}