<?php

namespace App\Models\DAO;

use App\Models\Entidades\Produto;
use App\Models\Entidades\Categoria;
use App\Lib\Upload;

class ProdutoDAO extends BaseDAO
{
    public  function listar($id = null)
    {
        if($id) {
            $resultado = $this->select(
                "SELECT * FROM produtos WHERE id_produto = $id"
            );

            return $resultado->fetchObject(Produto::class);
        }else{
            $resultado = $this->select(
                'SELECT * FROM produtos'
            );
            return $resultado->fetchAll(\PDO::FETCH_CLASS, Produto::class);
        }

        return false;
    }

    public  function salvar(Produto $produto) 
    {
        try {

            $nome           = $produto->getNome();
            $preco          = $produto->getPreco();
            $pdo_quantidade     = $produto->getQuantidade();
            $descricao      = $produto->getDescricao();
            $sku            = $produto->getSku();
            $categoria_id  = $produto->getCategoria();
            $image = $produto->getImage();

            $conn = mysqli_connect("localhost", "root", "", "webjump");

            $sqlInsert = mysqli_query($conn, "INSERT into produtos (nome,sku,pdo_descricao,pdo_quantidade,pdo_preco)
                           values ('" . $nome . "','" . $sku . "','" . $descricao . "','" . $pdo_quantidade . "','" . $preco . "')");
            $id_produto = mysqli_insert_id($conn);

            if(!empty($id_produto) && !empty($categoria_id)){


                $array = explode('|', $column[5]);

                    foreach($array as $key => $categoria){

                    $sqlInsertCategoria = mysqli_query($conn, "INSERT into categorias (nomeCategoria)
                           values ('" . $categoria . "')");

                    $id_categoria = mysqli_insert_id($conn);

                    if(!empty($id_produto) && !empty($id_categoria)){
                        $sqlInsertProdCat = mysqli_query($conn, "INSERT into produtos_categorias (produto_id, categoria_id)
                               values (" . $id_produto . "," . $id_categoria . " )");                        
                    }
                
                }

            }                
            return $id_produto;
            /*return $this->insert(
                'produtos',
                ":sku,:nome,:pdo_preco,:pdo_quantidade,:pdo_descricao",
                [
                    ':sku'=>$sku,
                    ':nome'=>$nome,
                    ':pdo_preco'=>$preco,
                    ':pdo_quantidade'=>$pdo_quantidade,
                    ':pdo_descricao'=>$descricao
                ]
            );*/


        }catch (\Exception $e){
            throw new \Exception("Erro na gravação de dados.", 500);
        }
    }

    public  function atualizar(Produto $produto) 
    {

        try {

            $id_produto     = $produto->getId();
            $sku            = $produto->getSku();
            $nome           = $produto->getNome();
            $preco          = $produto->getPreco();
            $pdo_quantidade     = $produto->getQuantidade();
            $descricao      = $produto->getDescricao();

            return $this->update(
                'produtos',
                "sku = :sku, nome = :nome, pdo_preco = :pdo_preco, pdo_quantidade = :pdo_quantidade, pdo_descricao = :pdo_descricao",
                [
                    ':id_produto'=>$id_produto,
                    ':sku'=>$sku,
                    ':nome'=>$nome,
                    ':pdo_preco'=>$preco,
                    ':pdo_quantidade'=>$pdo_quantidade,
                    ':pdo_descricao'=>$descricao
                ],
                "id_produto = :id_produto"
            );

        }catch (\Exception $e){
            throw new \Exception("Erro na gravação de dados.", 500);
        }
    }

    public function excluir(Produto $produto)
    {
        try {
            $id = $produto->getId();

            $excluir = self::excluirCategoriasProdutos($id);

            return $this->delete('produtos',"id_produto = $id");

        }catch (Exception $e){

            throw new \Exception("Erro ao deletar", 500);
        }
    }


    public function excluirCategoriasProdutos($id)
    {
        try {
            

            return $this->delete('produtos_categorias',"produto_id = $id");

        }catch (Exception $e){

            throw new \Exception("Erro ao deletar", 500);
        }
    }

    public  function listarCategorias($id = null)
        {
                $resultado = $this->select(
                    'SELECT DISTINCT(nomeCategoria) FROM categorias'
                );
                return $resultado->fetchAll(\PDO::FETCH_CLASS, Categoria::class);
        }  

    public function salvarCategoriaProduto( $produto){

       try {

            $ultimo = self::ultimoId();
            
            $id_produto     = $ultimo->produto_id;
            $categoria      = $produto->getCategoria();

            return $this->insert(
                'produtos_categorias',
                ":produto_id,:categoria_id",
                [
                    ':produto_id'=>$id_produto,
                    ':categoria_id'=>$categoria[0]
                ]
            );

        }catch (\Exception $e){
            throw new \Exception("Erro na gravação de dados.", 500);
        }
    }

    public function ultimoId(){

         $resultado = $this->select(
                    'SELECT max(id_produto) as produto_id FROM produtos'
                );
        return $resultado->fetchObject(Produto::class);
    }

}