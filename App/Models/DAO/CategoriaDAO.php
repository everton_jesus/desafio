<?php

namespace App\Models\DAO;

use App\Models\Entidades\Categoria;

class CategoriaDAO extends BaseDAO
{
    public  function listar($id = null)
    {
        if($id) {
            $resultado = $this->select(
                "SELECT * FROM categorias WHERE id_categoria = $id"
            );

            return $resultado->fetchObject(Categoria::class);
        }else{
            $resultado = $this->select(
                'SELECT DISTINCT(nomeCategoria) FROM categorias'
            );
            return $resultado->fetchAll(\PDO::FETCH_CLASS, Categoria::class);
        }

        return false;
    }

    public  function salvar(Categoria $categoria) 
    {
        try {

            $nome   = $categoria->getNomeCategoria();
            $code   = $categoria->getCode();

            return $this->insert(
                'categorias',
                ":nomeCategoria,:code",
                [
                    ':nomeCategoria'=>$nome,
                    ':code'=>$code
                ]
            );

        }catch (\Exception $e){
            throw new \Exception("Erro na gravação de dados.", 500);
        }
    }

    public  function atualizar(Categoria $categoria) 
    {

        try {

            $id_categoria  = $categoria->getId();
            $nome          = $categoria->getNomeCategoria();
            $code          = $categoria->getCode();

            return $this->update(
                'categorias',
                "nomeCategoria = :nomeCategoria, code = :code",
                [
                    ':id_categoria'=>$id_categoria,
                    ':nomeCategoria'=>$nome,
                    ':code'=>$code
                ],
                "id_categoria = :id_categoria"
            );

        }catch (\Exception $e){
            throw new \Exception("Erro na gravação de dados.", 500);
        }
    }

    public function excluir(Categoria $categoria)
    {
        try {
            $id = $categoria->getId();

            return $this->delete('categorias',"id_categoria = $id");

        }catch (Exception $e){

            throw new \Exception("Erro ao deletar", 500);
        }
    }
}