<?php

namespace App\Models\Validacao;

use \App\Models\Validacao\ResultadoValidacao;
use \App\Models\Entidades\Categoria;

class CategoriaValidador{

    public function validar(Categoria $categoria)
    {
        $resultadoValidacao = new ResultadoValidacao();

        if(empty($categoria->getNomeCategoria()))
        {
            $resultadoValidacao->addErro('nomeCategoria',"Nome: Este campo não pode ser vazio");
        }
        
        if(empty($categoria->getCode()))
        {
            $resultadoValidacao->addErro('code',"Code: Este campo não pode ser vazio");
        }

        return $resultadoValidacao;
    }
}