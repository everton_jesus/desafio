Para criar o mini crud utilizei o php 7.2 com mysql 5.7 rodando apache 2.4.35

Rodei o projeto no Laragon uma ferramenta semelhante ao Wamp/xampp a diferença que ele cria os hosts de forma automatica.

Criei um arquivo chamado dump.sql com as tabelas utilizadas no projeto
A conexão ficou com o padrão HOST "localhost" USER "root", SENHA "", BD "webjump");

Se tratando de apache 
No arquivo .htacces existe os redirecinamentos para tratar as urls amigaveis sei que no nginx precisa realizar uma alteração no acessando etc/nginx/sites-enabled/default ou o server definido para teste no repositorio.

Código do .htacces
RewriteEngine On

RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-l

RewriteRule ^(.+)$ index.php?url=$1 [QSA,L]

O ideal seria rodar a ferramenta no apache.

No codigo utilizei PDO e mysqli, não é tão bacana mas como trabalho hoje com uma versão antiga do php(5.3) no trabalho arrisquei experimentar kkk 

Na parte de importação de produtos quando executado, deixei mostrando os dados importados para verificar se a importação travaria.

# Você quer ser um desenvolvedor Backend na Web Jump?
Criamos esse teste para avaliar seus conhecimentos e habilidades como desenvolvedor backend.

# O teste
O desafio é desenvolver um sistema de gerenciamento de produtos. Esse sistema será composto de um cadastro de produtos e categorias. Os requisitos desse sistema estão listados nos tópicos abaixo.
Não existe certo ou errado, queremos saber como você se sai em situações reais como esse desafio.

# Instruções
- O foco principal do nosso teste é o backend. Para facilitar você poderá utilizar os arquivos html  disponíveis no diretório assets
- Crie essa aplicação como se fosse uma aplicação real, que seria utilizada pelo WebJump
- Não utilize nenhum Framework. 
- Fique à vontade para usar bibliotecas/componentes externos
- Seguir princípios **SOLID** 
- Utilize boas práticas de programação
- Utilize boas práticas de git
- Documentar como rodar o projeto
- Crie uma documentação simples comentando sobre as tecnologias, versões e soluções adotadas

# Requisitos
- O sistema deverá ser desenvolvido utilizando a linguagem PHP (de preferência a versão mais nova)
- Você deve criar um CRUD que permita cadastrar as seguintes informações:
	- **Produto**: Nome, SKU (Código), preço, descrição, quantidade e categoria (cada produto pode conter uma ou mais categorias)
	- **Categoria**: Código e nome.
- Salvar as informações necessárias em um banco de dados (relacional ou não), de sua escolha
- Criar um importador de produtos/categorias via CLI no formato  de CSV. Importar o arquivo disponibilizado no repositório (assets/import.csv).
- Gerar logs das ações (diferencial)
- Testes automatizados com informação da cobertura de testes (diferencial)
- Como um desafio adicional você pode implementar o upload de imagem no cadastro de produtos

# O que será avaliado
- Estrutura e organização do código e dos arquivos
- Soluções adotadas
- Tecnologias utilizadas
- Qualidade
- Padrões PSR, Design Patterns
- Enfim, tudo será observado e levado em conta

# Como iniciar o desenvolvimento
- Fork esse repositório na sua conta do BitBucket.
- Crie uma branch com o nome desafio

# Como enviar seu teste
Envie um email para [carreira@webjump.com.br] com o link do seu repositório

Qualquer dúvida sobre o teste, fique a vontade para entrar em contato conosco.
